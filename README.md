# CI Images

This project contains various docker images that can be used as base images in the CI pipeline.

Currently the project contains the following images:
* **registry.gitlab.com/alv-ch/jobroom/ci-images/node-jdk-chrome**
  * node v.10 + openjdk v8 + chrome + various utilities (git) 
